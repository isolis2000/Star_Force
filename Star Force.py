from tkinter import *
import os
import pygame as pg
import winsound

Instrucciones = '''Instrucciones'''
About = '''About'''


# -------------------------------Función de imágenes-------------------------------
def cargar_imagen(nombre):
    ruta = os.path.join('Img', nombre)
    img = PhotoImage(file=ruta)
    return img


# ------------------------------Función de canciones------------------------------
def cargar_cancion(nombre):
    ruta = os.path.join('Canciones', nombre)
    return winsound.PlaySound(ruta, winsound.SND_ASYNC)


def off():
    winsound.PlaySound(None, winsound.SND_ASYNC)


# -------------------------Pantalla de presentación (root)-------------------------
root = Tk()
root.title("F-Zero")
root.minsize(900, 506)
root.resizable(width=NO, height=NO)

c_root = Canvas(root, width=900, height=506, bg='black')
c_root.place(x=0, y=0)

fondo_root = cargar_imagen('Fondo.gif')
fondo = Label(c_root, image=fondo_root)
fondo.place(x=0, y=0, relwidth=1, relheight=1)


def config():
    root.withdraw()
    config = Toplevel()
    config.title('Configuración')
    config.minsize(800, 600)
    config.resizable(width=NO, height=NO)

    fondo_config = cargar_imagen('Fondoconfig.gif')
    Amy = cargar_imagen('Amy.gif')
    Bayonetta = cargar_imagen('Bayonetta.gif')
    Blood_Falcon = cargar_imagen('Blood_Falcon.gif')
    Captian_Falcon = cargar_imagen('Captian_Falcon.gif')
    DK = cargar_imagen('DK.gif')
    Falcon = cargar_imagen('Falcon.gif')
    Fox = cargar_imagen('Fox.gif')
    Krystal = cargar_imagen('Krystal.gif')
    Marth = cargar_imagen('Marth.gif')
    Nights = cargar_imagen('Nights.gif')
    Peppy = cargar_imagen('Peppy.gif')
    Pico = cargar_imagen('Pico.gif')
    ROB = cargar_imagen('ROB.gif')
    Samus = cargar_imagen('Samus.gif')
    Slippy = cargar_imagen('Slippy.gif')
    Snake = cargar_imagen('Snake.gif')
    Sonic = cargar_imagen('Sonic.gif')
    Strife = cargar_imagen('Strife.gif')
    Wolf = cargar_imagen('Wolf.gif')
    ZS_Samus = cargar_imagen('ZS_Samus.gif')

    c_config = Canvas(config,widht=400, height=600, bg='black')
    c_config.place(x=0, y=0)

    c_config.create_image(0, 0, image=fondo_config, anchor=NW)


# ---------------------------------Pantalla about----------------------------------
def ventana_about():
    """
    Instituto Tecnológico de Costa Rica
    Ingeniería en Computadores
    Lenguaje: Python 3.6.4
    Autor: Iván Solís Ávila 2018209698
    Versión: 1.0.0
    Fecha de última modificación: 02/05/2018
    Entradas: no presenta
    Restricciones: no presenta
    Salidas: ventana de acerca del autor"""
    root.withdraw()
    win_about = Toplevel()
    win_about.title("About")
    win_about.minsize(width=400, height=400)
    win_about.config(bg='white')

    def close_event():
        win_about.destroy()
        root.destroy()

    def back():
        global pausa
        pausa = True
        win_about.destroy()
        root.deiconify()

    win_about.protocol("WM_DELETE_WINDOW", close_event)

    lbl_about = Label(win_about, text=About, bg='white')
    lbl_about.pack()

    btn_back2 = Button(win_about, text='Atrás', command=back, bg='white', fg='green')
    btn_back2.pack()

    win_about.mainloop()


# ------------------------------Pantalla instrucciones-----------------------------
def instrucciones():
    """
    Instituto Tecnológico de Costa Rica
    Ingeniería en Computadores
    Lenguaje: Python 3.6.4
    Autor: Iván Solís Ávila 2018209698
    Versión: 1.0.0
    Fecha de última modificación: 02/05/2018
    Entradas: no presenta
    Restricciones: no presenta
    Salidas: ventana de instrucciones"""
    root.withdraw()
    ven_ins = Toplevel()
    ven_ins.title('Instrucciones')
    ven_ins.minsize(width=200, height=400)
    ven_ins.config(bg='grey')

    def close_event():
        ven_ins.destroy()
        root.deiconify()

    def back():
        ven_ins.destroy()
        root.deiconify()

    ven_ins.protocol('WM_DELETE_WINDOW', close_event)

    lbl_ins = Label(ven_ins, text=Instrucciones, font=('Times New Roman', 14), bg='grey')
    lbl_ins.pack()

    btn_back = Button(ven_ins, text='Atrás', command=back, bg='white', fg='green')
    btn_back.pack()

    ven_ins.mainloop()


# --------------------------------------Botones------------------------------------
btn_juego = Button(c_root, text='Jugar', bg='grey')
btn_juego.place(x=828, y=320)

btn_about = Button(c_root, text='About', command=ventana_about, bg='grey')
btn_about.place(x=823, y=360)

btn_instrucciones = Button(c_root, text='Instrucciones', command=instrucciones, bg='grey')
btn_instrucciones.place(x=786, y=400)

btn_salir = Button(c_root, text='Salir Juego', command=root.destroy, bg='grey')
btn_salir.place(x=800, y=440)

root.mainloop()